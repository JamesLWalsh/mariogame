#include "Distance.h"

Distance::Distance(double nanometers) {
    this->nanometers = nanometers;
}

double Distance::toNanometers() {
    return this->nanometers;
}

double Distance::toMicrometers() {
    return this->nanometers / NANOS_IN_A_MICROMETER;
}

double Distance::toMillimeters() {
    return this->nanometers / NANOS_IN_A_MILLIMETER;
}

double Distance::toCentimeters() {
    return this->nanometers / NANOS_IN_A_CENTIMETER;
}

double Distance::toDecimeters() {
    return this->nanometers / NANOS_IN_A_DECIMETER;
}

double Distance::toMeters() {
    return this->nanometers / NANOS_IN_A_METER;
}

double Distance::toDecameters() {
    return this->nanometers / NANOS_IN_A_DECAMETER;
}

double Distance::toHectometers() {
    return this->nanometers / NANOS_IN_A_HECTOMETER;
}

double Distance::toKilometers() {
    return this->nanometers / NANOS_IN_A_KILOMETER;
}

Distance Distance::FromNanometers(double nanometers) {
    return Distance(nanometers);
}

Distance Distance::FromMicrometers(double micrometers) {
    return Distance(NANOS_IN_A_MICROMETER * micrometers);
}

Distance Distance::FromMillimeters(double millimeters) {
    return Distance(NANOS_IN_A_MILLIMETER * millimeters);
}

Distance Distance::FromCentimeters(double centimeters) {
    return Distance(NANOS_IN_A_CENTIMETER * centimeters);
}

Distance Distance::FromDecimeters(double decimeters) {
    return Distance(NANOS_IN_A_DECIMETER * decimeters);
}

Distance Distance::FromMeters(double meters) {
    return Distance(NANOS_IN_A_METER * meters);
}

Distance Distance::FromDecameters(double decameters) {
    return Distance(NANOS_IN_A_DECAMETER * decameters);
}

Distance Distance::FromHectometers(double hectometers) {
    return Distance(NANOS_IN_A_HECTOMETER * hectometers);
}

Distance Distance::FromKilometers(double kilometers) {
    return Distance(NANOS_IN_A_KILOMETER * kilometers);
}