#pragma once
#include "units/Point.h"
#include "units/Distance.h"

typedef struct Collider {
    Point location;
    Distance width;
    Distance height;
} Collider;