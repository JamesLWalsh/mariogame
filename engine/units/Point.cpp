#include <math.h>
#include <cmath>
#include "Point.h"

Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}

Point Point::FromCartesian(double x, double y) {
    return Point(x, y);
}

const double Point::getX() {
    return x;
}

const double Point::getY() {
    return y;
}

void Point::setX(double x) {
    this->x = x;
}

void Point::setY(double y) {
    this->y = y;
}

Point Point::operator+(Point point) {
    return Point::FromCartesian(this->x + point.getX(), this->y + point.getY());
}

Point Point::operator-(Point point) {
    return Point::FromCartesian(this->x - point.getX(), this->y - point.getY());
}

Point Point::operator*(double multiplier) {
    return Point::FromCartesian(this->x * multiplier, this->y * multiplier);
}

Distance Point::distanceTo(Point point) {
    double xSquared = pow(this->getX() - point.getX(), 2);
    double ySquared = pow(this->getY() - point.getY(), 2);

    return Distance::FromMeters(sqrt(xSquared + ySquared));
}

Distance Point::horizontalDistanceTo(Point point) {
    return Distance::FromMeters(std::abs(this->getX() - point.getX()));
}

Distance Point::verticalDistanceTo(Point point) {
    return Distance::FromMeters(std::abs(this->getY() - point.getY()));
}