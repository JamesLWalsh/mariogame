#pragma once

#include "Distance.h"

class Point {
public:
    static Point FromCartesian(double x, double y);
    const double getX();
    const double getY();
    void setX(double x);
    void setY(double y);
    Point operator+(Point point);
    Point operator-(Point point);
    Point operator*(double multiplier);
    Distance distanceTo(Point point);
    Distance horizontalDistanceTo(Point point);
    Distance verticalDistanceTo(Point point);
private:
    Point(double x, double y);
    double x, y;
};
