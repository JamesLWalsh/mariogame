#include "Duration.h"

Duration::Duration(float nanoseconds) {
    this->nanoseconds = nanoseconds;
}

float Duration::toNanoseconds() {
    return this->nanoseconds;
}

float Duration::toMicroseconds() {
    return this->nanoseconds / NANOS_IN_A_MICROSECOND;
}

float Duration::toMilliseconds() {
    return this->nanoseconds / NANOS_IN_A_MILLISECOND;
}

float Duration::toSeconds() {
    return this->nanoseconds / NANOS_IN_A_SECOND;
}

float Duration::toMinutes() {
    return this->nanoseconds / NANOS_IN_A_MINUTE;
}

float Duration::toHours() {
    return this->nanoseconds / NANOS_IN_AN_HOUR;
}

float Duration::toDays() {
    return this->nanoseconds / NANOS_IN_A_DAY;
}

float Duration::toWeeks() {
    return this->nanoseconds / NANOS_IN_A_WEEK;
}

float Duration::toMonths() {
    return this->nanoseconds / NANOS_IN_A_MONTH;
}

float Duration::toYears() {
    return this->nanoseconds / NANOS_IN_A_YEAR;
}

Duration Duration::subtract(Duration duration) {
    return Duration::FromNanoseconds(nanoseconds - duration.toNanoseconds());
}

Duration Duration::operator-(Duration duration) {
    return this->subtract(duration);
}

Duration Duration::add(Duration duration) {
    return Duration::FromNanoseconds(nanoseconds + duration.toNanoseconds());
}

Duration Duration::operator+(Duration duration) {
    return this->add(duration);
}

bool Duration::isGreaterThan(Duration duration) {
    return this->nanoseconds > duration.toNanoseconds();
}

bool Duration::operator>(Duration duration) {
    return this->isGreaterThan(duration);
}

bool Duration::isEqualOrGreaterThan(Duration duration) {
    return this->nanoseconds >= duration.toNanoseconds();
}

bool Duration::operator>=(Duration duration) {
    return this->isEqualOrGreaterThan(duration);
}

bool Duration::isLessThan(Duration duration) {
    return this->nanoseconds < duration.toNanoseconds();
}

bool Duration::operator<(Duration duration) {
    return this->isLessThan(duration);
}

bool Duration::isEqualOrLessThan(Duration duration) {
    return this->nanoseconds <= duration.toNanoseconds();
}

bool Duration::operator<=(Duration duration) {
    return this->isEqualOrLessThan(duration);
}

Duration Duration::FromNanoseconds(float nanoseconds) {
    return Duration(nanoseconds);
}

Duration Duration::FromMicroseconds(float microseconds) {
    return Duration(microseconds * NANOS_IN_A_MICROSECOND);
}

Duration Duration::FromMilliseconds(float milliseconds) {
    return Duration(milliseconds * NANOS_IN_A_MILLISECOND);
}

Duration Duration::FromSeconds(float seconds) {
    return Duration(seconds * NANOS_IN_A_SECOND);
}

Duration Duration::FromMinutes(float minutes) {
    return Duration(minutes * NANOS_IN_A_MINUTE);
}

Duration Duration::FromHours(float hours) {
    return Duration(hours * NANOS_IN_AN_HOUR);
}

Duration Duration::FromDays(float days) {
    return Duration(days * NANOS_IN_A_DAY);
}

Duration Duration::FromWeeks(float weeks) {
    return Duration(weeks * NANOS_IN_A_WEEK);
}

Duration Duration::FromMonths(float months) {
    return Duration(months * NANOS_IN_A_MONTH);
}

Duration Duration::FromYears(float years) {
    return Duration(years * NANOS_IN_A_YEAR);
}