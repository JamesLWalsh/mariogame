#pragma once

class Duration {
public:
    float toNanoseconds();
    float toMicroseconds();
    float toMilliseconds();
    float toSeconds();
    float toMinutes();
    float toHours();
    float toDays();
    float toWeeks();
    float toMonths();
    float toYears();
    Duration subtract(Duration duration);
    Duration add(Duration duration);
    Duration operator + (Duration duration);
    Duration operator - (Duration duration);

    bool isGreaterThan(Duration duration);
    bool operator > (Duration duration);

    bool isEqualOrGreaterThan(Duration duration);
    bool operator >= (Duration duration);

    bool isLessThan(Duration duration);
    bool operator < (Duration duration);

    bool isEqualOrLessThan(Duration duration);
    bool operator <= (Duration duration);

    static Duration FromNanoseconds(float nanoseconds);
    static Duration FromMicroseconds(float microseconds);
    static Duration FromMilliseconds(float milliseconds);
    static Duration FromSeconds(float seconds);
    static Duration FromMinutes(float minutes);
    static Duration FromHours(float hours);
    static Duration FromDays(float days);
    static Duration FromWeeks(float weeks);
    static Duration FromMonths(float months);
    static Duration FromYears(float years);
private:
    static constexpr float WEEKS_IN_A_MONTH = 4.34812f;

    static constexpr float NANOS_IN_A_MICROSECOND = 1000;
    static constexpr float NANOS_IN_A_MILLISECOND = 1000 * NANOS_IN_A_MICROSECOND;

    static constexpr float NANOS_IN_A_SECOND = 1000 * NANOS_IN_A_MILLISECOND;
    static constexpr float NANOS_IN_A_MINUTE = 60 * NANOS_IN_A_SECOND;
    static constexpr float NANOS_IN_AN_HOUR = 60 * NANOS_IN_A_MINUTE;

    static constexpr float NANOS_IN_A_DAY = 24 * NANOS_IN_AN_HOUR;
    static constexpr float NANOS_IN_A_WEEK = 7 * NANOS_IN_A_DAY;
    static constexpr float NANOS_IN_A_MONTH = WEEKS_IN_A_MONTH * NANOS_IN_A_WEEK;
    static constexpr float NANOS_IN_A_YEAR = 12 * NANOS_IN_A_MONTH;

    Duration(float nanoseconds);
    float nanoseconds;
};