#include "Input.h"

bool Input::getInputState(int inputIndex) {
    return this->inputStates[inputIndex];
}

void Input::setInputState(int inputIndex, bool state) {
    this->inputStates[inputIndex] = state;
}