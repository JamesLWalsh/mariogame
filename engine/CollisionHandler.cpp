#include "CollisionHandler.h"
#include "CollisionEvaluator.h"
#include <math.h>
#include <algorithm>
#include <iostream>

CollisionHandler::CollisionHandler(Distance regionWidth, Distance overlapBetweenRegions, bool performCollisionsOnDynamicColliders): regionWidth(regionWidth), overlapBetweenRegions(overlapBetweenRegions) {
    this->performCollisionsOnDynamicColliders = performCollisionsOnDynamicColliders;
}

void CollisionHandler::addStaticCollider(ICollidable* collidable) {
    std::vector<int> concernedRegions = this->findRegionsConcerningCollidable(collidable);

    for(auto region : concernedRegions) {
        this->addStaticColliderInRegion(region, collidable);
    }
}

void CollisionHandler::addDynamicCollider(ICollidable* collidable) {
    this->dynamicColliders.push_back(collidable);
}

void CollisionHandler::addStaticColliderInRegion(int region, ICollidable* collidable) {
    if(!this->regions.count(region)) {
        this->regions[region] = { std::vector<ICollidable*>() };
    }

    this->regions[region].colliders.push_back(collidable);
}

std::vector<int> CollisionHandler::findRegionsConcerningCollidable(ICollidable *collidable) {
    double regionWidthInMeters = this->regionWidth.toMeters();
    double colliderPositionX = collidable->getCollider().location.getX();

    int positionInRegions = (int) floor(colliderPositionX / regionWidthInMeters);

    double regionStartX = positionInRegions * regionWidthInMeters;
    double regionEndX = (positionInRegions + 1) * regionWidthInMeters;

    std::vector<int> regions = { positionInRegions };

    if(colliderPositionX - regionStartX <= this->overlapBetweenRegions.toMeters()) {
        regions.push_back(positionInRegions - 1);
    }

    if(regionEndX - colliderPositionX <= this->overlapBetweenRegions.toMeters()) {
        regions.push_back(positionInRegions + 1);
    }

    return regions;
}

void CollisionHandler::performCollisions() {
    std::map<ICollidable*, std::vector<ICollidable*>> dynamicOnDynamicCollisions;

    for (auto &collidable : this->dynamicColliders) {
        this->performDynamicOnStaticCollisions(collidable);

        if(this->performCollisionsOnDynamicColliders) {
            this->performDynamicOnDynamicCollisions(collidable, dynamicOnDynamicCollisions);
        }
    }
}

void CollisionHandler::performDynamicOnStaticCollisions(ICollidable* collidable) {
    std::vector<int> concernedRegions = this->findRegionsConcerningCollidable(collidable);

    for(auto region : concernedRegions) {
        for(auto &staticCollidable : this->regions[region].colliders) {
            Collision collision = CollisionEvaluator::Evaluate(collidable->getCollider(), staticCollidable->getCollider());

            if(collision.side != NONE) {
                collidable->onCollision(staticCollidable, collision);
                staticCollidable->onCollision(collidable, collision);
            }
        }
    }
}

void CollisionHandler::performDynamicOnDynamicCollisions(ICollidable *collidable, std::map<ICollidable*, std::vector<ICollidable*>> &dynamicOnDynamicCollisions) {
    for(auto &dynamicCollider : this->dynamicColliders) {
        if(collidable != dynamicCollider) {
            std::vector<ICollidable*> colliders = dynamicOnDynamicCollisions[dynamicCollider];

            if(std::find(colliders.begin(), colliders.end(), collidable) == colliders.end()) {
                Collision collision = CollisionEvaluator::Evaluate(collidable->getCollider(), dynamicCollider->getCollider());

                if(collision.side != NONE) {
                    collidable->onCollision(dynamicCollider, collision);

                    this->invertCollision(collision);

                    dynamicCollider->onCollision(collidable, collision);
                }

                dynamicOnDynamicCollisions[collidable].push_back(dynamicCollider);
            }
        }
    }
}

void CollisionHandler::clearStaticColliders() {
    for(auto &pair : this->regions) {
        pair.second.colliders.clear();
    }

    this->regions.clear();
}

void CollisionHandler::clearDynamicColliders() {
    this->dynamicColliders.clear();
}

void CollisionHandler::removeDynamicCollider(ICollidable *collidable) {
    for(int i = 0; i < this->dynamicColliders.size(); i++) {
        if(this->dynamicColliders[i] == collidable) {
            this->dynamicColliders.erase(this->dynamicColliders.begin() + i);
        }
    }
}

void CollisionHandler::invertCollision(Collision &collision) {
    if(collision.side == LEFT) {
        collision.side = RIGHT;
    }

    if(collision.side == RIGHT) {
        collision.side = LEFT;
    }

    if(collision.side == TOP) {
        collision.side = BOTTOM;
    }

    if(collision.side == BOTTOM) {
        collision.side = TOP;
    }
}