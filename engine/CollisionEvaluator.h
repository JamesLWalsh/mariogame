#pragma once
#include "Collision.h"
#include "ICollidable.h"

class CollisionEvaluator {
public:
    static Collision Evaluate(Collider a, Collider b);
};