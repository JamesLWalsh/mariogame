#include <cmath>
#include <iostream>
#include "CollisionEvaluator.h"

Collision CollisionEvaluator::Evaluate(Collider colliderA, Collider colliderB) {
    double dx = (colliderA.location.getX() + colliderA.width.toMeters() / 2) - (colliderB.location.getX() + colliderB.width.toMeters() / 2);
    double dy = (colliderA.location.getY() + colliderA.height.toMeters() / 2) - (colliderB.location.getY() + colliderB.height.toMeters() / 2);

    double width = (colliderA.width.toMeters() + colliderB.width.toMeters()) / 2;
    double height = (colliderA.height.toMeters() + colliderB.height.toMeters()) / 2;

    if(std::abs(dx) < (width - 1) && std::abs(dy) < height) {
        double crossWidth = width * dy;
        double crossHeight = height * dx;

        if(crossWidth > crossHeight) {
            return { crossWidth > -crossHeight ? TOP : RIGHT };
        } else {
            return { crossWidth > -crossHeight ? LEFT : BOTTOM };
        }
    }

    return { NONE };
}
