#pragma once
#include <vector>
#include "../ITile.h"
#include "../units/Duration.h"

class AnimatedTile : public ITile {
public:
    AnimatedTile(std::vector<int> textures, Duration textureDuration);
    int getTexture();

private:
    std::vector<int> textures;
    int currentTextureIndex = 0;
    Duration lastSwitchTime;
    Duration textureDuration;
};