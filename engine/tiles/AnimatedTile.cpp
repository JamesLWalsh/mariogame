#include "AnimatedTile.h"
#include <time.h>

AnimatedTile::AnimatedTile(std::vector<int> textures, Duration textureDuration) : lastSwitchTime(Duration::FromSeconds(0)), textureDuration(textureDuration) {
    this->textures = textures;
}

int AnimatedTile::getTexture() {
    Duration currentTime = Duration::FromSeconds(clock() / CLOCKS_PER_SEC);

    if(currentTime - this->lastSwitchTime > this->textureDuration) {
        this->currentTextureIndex++;

        if(this->currentTextureIndex >= this->textures.size()) {
            this->currentTextureIndex = 0;
        }

        this->lastSwitchTime = currentTime;
    }

    return this->textures[this->currentTextureIndex];
}
