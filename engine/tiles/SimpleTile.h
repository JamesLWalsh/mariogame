#pragma once
#include "../ITile.h"

class SimpleTile: public ITile {
public:
    SimpleTile(int texture);
    int getTexture();
private:
    int texture;
};
