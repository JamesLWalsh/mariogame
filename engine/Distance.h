#pragma once

class Distance {
public:
    double toNanometers();
    double toMicrometers();
    double toMillimeters();
    double toCentimeters();
    double toDecimeters();
    double toMeters();
    double toDecameters();
    double toHectometers();
    double toKilometers();

    static Distance FromNanometers(double nanometers);
    static Distance FromMicrometers(double micrometers);
    static Distance FromMillimeters(double millimeters);
    static Distance FromCentimeters(double centimeters);
    static Distance FromDecimeters(double decimeters);
    static Distance FromMeters(double meters);
    static Distance FromDecameters(double decameters);
    static Distance FromHectometers(double hectometers);
    static Distance FromKilometers(double kilometers);
private:
    Distance(double nanometers);
    double nanometers;

    static constexpr double NANOS_IN_A_MICROMETER = 1000;
    static constexpr double NANOS_IN_A_MILLIMETER = NANOS_IN_A_MICROMETER * 1000;
    static constexpr double NANOS_IN_A_CENTIMETER = NANOS_IN_A_MILLIMETER * 10;
    static constexpr double NANOS_IN_A_DECIMETER = NANOS_IN_A_CENTIMETER * 10;
    static constexpr double NANOS_IN_A_METER = NANOS_IN_A_DECIMETER * 10;
    static constexpr double NANOS_IN_A_DECAMETER = NANOS_IN_A_METER * 10;
    static constexpr double NANOS_IN_A_HECTOMETER = NANOS_IN_A_DECAMETER * 10;
    static constexpr double NANOS_IN_A_KILOMETER = NANOS_IN_A_HECTOMETER * 10;
};