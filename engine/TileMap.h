#pragma once
#include <vector>
#include "ITile.h"
#include "ITileRenderer.h"
#include "units/Point.h"
#include "Viewport.h"

class TileMap {
public:
    TileMap(unsigned int width, unsigned int height, ITile* defaultTile);
    TileMap(std::vector<std::vector<ITile*>> tiles);
    void render(ITileRenderer* renderer, Point origin, Viewport viewport);
private:
    std::vector<std::vector<ITile*>> map;
    unsigned int width, height;
};