#include <iostream>
#include "TileMap.h"
#include "tiles/SimpleTile.h"

TileMap::TileMap(unsigned int width, unsigned int height, ITile* defaultTile) {
    std::vector<std::vector<ITile*>> map;

    for(unsigned int x = 0; x < width; x++) {
        std::vector<ITile*> row;
        for(unsigned int y = 0; y < height; y++) {
            row.push_back(defaultTile);
        }

        map.push_back(row);
    }

    this->map = map;
    this->width = width;
    this->height = height;
}

TileMap::TileMap(std::vector<std::vector<ITile*>> tiles) {
    this->map = tiles;
    this->width = tiles.size();
    this->height = tiles[0].size();
}

void TileMap::render(ITileRenderer* renderer, Point origin, Viewport viewport) {
    auto startX = (unsigned int) viewport.start.getX();
    auto startY = (unsigned int) viewport.start.getY();

    for(unsigned int x = startX; x < startX + viewport.width.toMeters(); x++) {
        if(x >= this->map.size())
            break;

        for(unsigned int y = startY; y < startY + viewport.height.toMeters(); y++) {
            if(y >= this->map[x].size())
                break;

            renderer->renderTile(this->map[x][y], Point::FromCartesian(x, y), origin);
        }
    }
}