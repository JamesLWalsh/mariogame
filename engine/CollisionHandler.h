#pragma once
#include "ICollidable.h"
#include "units/Distance.h"
#include <vector>
#include <map>

class CollisionHandler {
public:
    CollisionHandler(Distance regionWidth, Distance overlapBetweenRegions, bool performCollisionsOnDynamicColliders);
    void removeDynamicCollider(ICollidable* collidable);
    void addStaticCollider(ICollidable* collidable);
    void addDynamicCollider(ICollidable* collidable);
    void performCollisions();

    void clearStaticColliders();
    void clearDynamicColliders();
private:
    void invertCollision(Collision &collision);
    void addStaticColliderInRegion(int region, ICollidable* collidable);
    std::vector<int> findRegionsConcerningCollidable(ICollidable* collidable);

    void performDynamicOnStaticCollisions(ICollidable* collidable);
    void performDynamicOnDynamicCollisions(ICollidable* collidable, std::map<ICollidable*, std::vector<ICollidable*>> &dynamicOnDynamicCollisions);

    typedef struct Region {
        std::vector<ICollidable*> colliders;
    } Region;

    bool performCollisionsOnDynamicColliders;
    Distance regionWidth;
    Distance overlapBetweenRegions;

    std::map<int, Region> regions;
    std::vector<ICollidable*> dynamicColliders;
};