#pragma once

enum Side {
    RIGHT,
    LEFT,
    TOP,
    BOTTOM,
    NONE
};

typedef struct Collision {
    Side side;
} Collision;