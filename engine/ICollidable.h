#pragma once
#include <string>
#include "Collider.h"
#include "Collision.h"

class ICollidable {
public:
    ~ICollidable() {};
    virtual Collider getCollider() = 0;
    virtual void onCollision(ICollidable *collider, Collision collision) = 0;
    virtual std::string getTag() = 0;
};