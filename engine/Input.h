#pragma once
#include <map>

class Input {
public:
    void setInputState(int inputIndex, bool state);
    bool getInputState(int inputIndex);
private:
    std::map<int, bool> inputStates;
};