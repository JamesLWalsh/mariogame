#pragma once
#include "ITile.h"
#include "units/Point.h"

class ITileRenderer {
public:
    virtual void renderTile(ITile* tile, Point tilePosition, Point offset) = 0;
    virtual ~ITileRenderer() {};
};