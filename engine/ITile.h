#pragma once

class ITile {
public:
    ~ITile() {};
    virtual int getTexture() = 0;
};