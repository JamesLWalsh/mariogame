#pragma once
#include "units/Point.h"

typedef struct Viewport {
    Point start;
    Distance width;
    Distance height;
} Viewport;