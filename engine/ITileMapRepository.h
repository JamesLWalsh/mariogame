#pragma once
#include <string>
#include "TileMap.h"

class ITileMapRepository {
public:
    ~ITileMapRepository() {};
    virtual TileMap loadMap(std::string mapName) = 0;
};