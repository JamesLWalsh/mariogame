#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "../engine/ITile.h"

class SFMLTileSet {
public:
    SFMLTileSet(sf::Image tileSet, unsigned int tileSize);
    sf::Texture* getTexture(ITile* tile);
    unsigned int getTileSize();
private:
    unsigned int tileSize;
    std::vector<sf::Texture*> textures;
};

