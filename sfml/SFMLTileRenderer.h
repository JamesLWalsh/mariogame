#pragma once
#include "../engine/TileMap.h"
#include "SFMLTileSet.h"
#include "../engine/ITileRenderer.h"

class SFMLTileRenderer: public ITileRenderer {
public:
    SFMLTileRenderer(SFMLTileSet tileSet, sf::RenderWindow* window);
    void renderTile(ITile* tile, Point tilePosition, Point offset);
private:
    std::map<ITile*, sf::Texture*> textureCache;
    sf::Sprite* sprite;
    SFMLTileSet tileSet;
    sf::RenderWindow* window;
};