#include <SFML/Graphics.hpp>
#include "SFMLTileRenderer.h"

SFMLTileRenderer::SFMLTileRenderer(SFMLTileSet tileSet, sf::RenderWindow* window) : tileSet(tileSet){
    this->window = window;
    this->sprite = new sf::Sprite;
}

void SFMLTileRenderer::renderTile(ITile* tile, Point tilePosition, Point offset) {
    Point finalPosition = offset + tilePosition * this->tileSet.getTileSize();
    this->sprite->setTexture(*this->tileSet.getTexture(tile));
    this->sprite->setPosition(finalPosition.getX(), finalPosition.getY());

    this->window->draw(*this->sprite);
}