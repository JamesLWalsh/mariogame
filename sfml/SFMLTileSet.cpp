#include "SFMLTileSet.h"

SFMLTileSet::SFMLTileSet(sf::Image tileSet, unsigned int tileSize) {
    sf::Vector2u tileSetDimensions = tileSet.getSize();
    unsigned int rows = tileSetDimensions.y / tileSize;
    unsigned int columns = tileSetDimensions.x / tileSize;

    for(unsigned int x = 0; x < columns; x++) {
        for(unsigned int y = 0; y < rows; y++) {
            sf::IntRect cropRect(x * tileSize, y * tileSize, tileSize, tileSize);
            auto* texture = new sf::Texture();
            texture->loadFromImage(tileSet, cropRect);

            this->textures.push_back(texture);
        }
    }

    this->tileSize = tileSize;
}

unsigned int SFMLTileSet::getTileSize() {
    return this->tileSize;
}

sf::Texture* SFMLTileSet::getTexture(ITile* tile) {
    return this->textures[tile->getTexture()];
}
