# MARIO GAME #

Made using the following stack:

- C++

- SFML

- CMake

## Notes
In order for CMake to locate SFML on Windows, set the environment variable *SFML_ROOT* to the folder where SFML is installed.