#pragma once
#include <SFML/Graphics.hpp>
#include "../engine/units/Point.h"

class ScoreBoard {
public:
    ScoreBoard();
    void increaseCoins();
    void switchWorld(std::string newWorld);
    void render(sf::RenderWindow &window, Point lowerBound);
    void onGameComplete();
    void reset();
private:
    bool hasCompletedGame = false;
    int coins = 0;
    sf::Sprite coin;
    sf::Font renderFont;
    sf::Text worldText;
    sf::Text coinsText;
    sf::Text gameCompletedText;

    Point WORLD_TEXT_POSITION = Point::FromCartesian(200, 8);
    Point COINS_TEXT_POSITION = Point::FromCartesian(425, 8);
    Point COINS_ICON_POSITION = Point::FromCartesian(400, 10);
    Point GAME_COMPLETED_TEXT_POSITION = Point::FromCartesian(150, 200);

};