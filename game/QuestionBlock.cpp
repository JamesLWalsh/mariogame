#include <time.h>
#include <iostream>
#include "QuestionBlock.h"
#include "GameTags.h"
#include "Mushroom.h"

QuestionBlock::QuestionBlock(Point position, Distance size): position(position), size(size) {}

Collider QuestionBlock::getCollider() {
    return { this->position, this->size, this->size };
}

void QuestionBlock::onCollision(ICollidable *collider, Collision collision) {
    if(!this->triggered && collider->getTag() == PLAYER) {
        if(collision.side == TOP) {
            this->triggered = true;
        }
    }
}

std::string QuestionBlock::getTag() {
    return QUESTION;
}

int QuestionBlock::getTexture() {
    if(this->triggered) {
        return T_QUESTION_TRIGGERED;
    }

    Duration currentTime = Duration::FromSeconds(clock() / CLOCKS_PER_SEC);

    if(currentTime - this->lastSwitchTime > this->textureDuration) {
        this->currentTextureIndex++;

        if(this->currentTextureIndex >= this->textures.size()) {
            this->currentTextureIndex = 0;
        }

        this->lastSwitchTime = currentTime;
    }

    return this->textures[this->currentTextureIndex];
}

void QuestionBlock::update(EntityManager* entityManager, CollisionHandler* collisionHandler) {
    if(this->triggered && !this->hasSpawnedMushroom) {
        Mushroom* mushroom = new Mushroom(this->position);
        entityManager->addVisibleEntity(mushroom);
        collisionHandler->addDynamicCollider(mushroom);
        this->hasSpawnedMushroom = true;
    }
}
