#include <iostream>
#include "Mushroom.h"
#include "GameTags.h"

Mushroom::Mushroom(Point spawnPosition) : VisibleEntity(spawnPosition, size, size){
    Duration currentTime = Duration::FromSeconds(clock() / CLOCKS_PER_SEC);
    this->direction = (int) currentTime.toSeconds() % 2 == 0 ? Point::FromCartesian(-MOVE_SPEED, -FALL_SPEED) : Point::FromCartesian(MOVE_SPEED, -FALL_SPEED);

    sf::Texture* texture = new sf::Texture;
    texture->loadFromFile("resources/mushroom.png");
    this->size = Distance::FromMeters(texture->getSize().x);

    sf::Sprite* sprite = new sf::Sprite(*texture);
    sprite->setPosition(spawnPosition.getX(), spawnPosition.getY());
    this->sprite = sprite;
}

void Mushroom::update(Duration deltaTime) {
    if(this->hasGoneUp) {
        this->position = this->position - this->direction * deltaTime.toMilliseconds();
    } else {
        Point movement = Point::FromCartesian(0, RAISE_SPEED) * deltaTime.toMilliseconds();
        this->position = this->position - movement;

        this->verticalMovement = this->verticalMovement + movement;

        if(this->verticalMovement.getY() >= this->size.toMeters()) {
            this->hasGoneUp = true;
        }
    }
}

void Mushroom::render(sf::RenderWindow &window) {
    this->sprite->setPosition(this->position.getX(), this->position.getY());
    window.draw(*this->sprite);
}

void Mushroom::onCollision(ICollidable *collider, Collision collision) {
    if(collider->getTag() == PLAYER) {
        this->hasBeenTaken = true;
        this->position = Point::FromCartesian(-10000, 0);
    }

    if(this->hasGoneUp && (collider->getTag() == BLOCK || collider->getTag() == QUESTION)) {
        if(collision.side == BOTTOM) {
            this->position = Point::FromCartesian(this->position.getX(), collider->getCollider().location.getY() - this->size.toMeters());
        }

        if(collision.side == LEFT) {
            this->position = Point::FromCartesian(collider->getCollider().location.getX() + collider->getCollider().width.toMeters(), this->position.getY());
        }

        if(collision.side == RIGHT) {
            this->position = Point::FromCartesian(collider->getCollider().location.getX() - this->size.toMeters(), this->position.getY());
        }
    }
}

std::string Mushroom::getTag() {
    return MUSHROOM;
}

Collider Mushroom::getCollider() {
    return { this->position, this->size, this->size };
}

bool Mushroom::isTerminated() {
    return this->hasBeenTaken;
}