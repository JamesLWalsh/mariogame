#include "VisibleEntity.h"

class Coin : public VisibleEntity {
public:
    Coin(Point spawnPosition);
    void update(Duration deltaTime);
    void render(sf::RenderWindow &window);
    bool isTerminated();
    void onCollision(ICollidable *collider, Collision collision);
    std::string getTag();
private:
    bool collected = false;
    sf::Sprite coinSprite;
};