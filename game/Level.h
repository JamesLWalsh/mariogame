#pragma once
#include <vector>
#include "../engine/ICollidable.h"
#include "../engine/TileMap.h"
#include "Entity.h"
#include "QuestionBlock.h"
#include "Plant.h"

typedef struct LevelList {
    struct LevelList* next;
    std::string name;
} LevelList;

typedef struct Level {
    std::vector<ICollidable*> staticColliders;
    std::vector<ICollidable*> dynamicColliders;
    std::vector<VisibleEntity*> visibleEntities;
    std::vector<Spawner*> entitySpawners;
    TileMap* map;
    std::string name;
} Level;