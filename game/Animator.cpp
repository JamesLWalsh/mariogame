#include <iostream>
#include "Animator.h"

Animator::Animator(std::vector<AnimationState *> animations) {
    this->animationStates = animations;
}

sf::Texture* Animator::getActiveTexture() {
    return this->currentState->currentAnimation->texture;
}

void Animator::update(Duration deltaTime) {
    this->timeSinceSwitch = this->timeSinceSwitch + deltaTime;

    if(this->timeSinceSwitch >= this->currentState->animationDuration) {
        this->currentState->currentAnimation = this->currentState->currentAnimation->next;
        this->timeSinceSwitch = Duration::FromSeconds(0);
    }
}

void Animator::switchState(int stateName) {
    for(auto animationState : this->animationStates) {
        if(animationState->stateName == stateName) {
            this->currentState = animationState;
        }
    }
}
