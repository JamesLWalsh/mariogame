#include "BlockCollider.h"
#include "GameTags.h"
#include "EntityManager.h"

BlockCollider::BlockCollider(Point position, Distance size): position(position), size(size) {
}

Collider BlockCollider::getCollider() {
    return { this->position, this->size, this->size };
}

void BlockCollider::onCollision(ICollidable *collider, Collision collision) {}

std::string BlockCollider::getTag() {
    return BLOCK;
}
