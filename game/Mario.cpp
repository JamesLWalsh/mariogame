#include <iostream>
#include "Mario.h"
#include "GameTags.h"
#include "TextureUtils.h"
#include "AnimationBuilder.h"
#include "EntityManager.h"

enum AnimationStates {
    IDLE,
    WALKING_LEFT,
    WALKING_RIGHT,
};

Mario::Mario(ScoreBoard* scoreBoard): position(Point::FromCartesian(0, 0)),
                lowestPosition(Point::FromCartesian(0, 0)),
                width(Distance::FromMeters(0)),
                height(Distance::FromMeters(0)) {
    this->scoreBoard = scoreBoard;
    sf::Image spriteSheet;
    spriteSheet.loadFromFile("resources/players/mario.png");

    sf::IntRect idleRect(32, 16, 32, 64);
    sf::IntRect walking1Rect(0, 16, 32, 64);
    sf::IntRect walking2Rect(32, 16, 32, 64);
    sf::IntRect walking3Rect(64, 20, 32, 60);

    AnimationState* idle = AnimationBuilder::AssembleAnimationState(IDLE, Duration::FromSeconds(10), spriteSheet, std::vector<sf::IntRect> { idleRect }, false);
    AnimationState* walkingRight = AnimationBuilder::AssembleAnimationState(WALKING_RIGHT, Duration::FromSeconds(1.5), spriteSheet, std::vector<sf::IntRect> { walking1Rect, walking2Rect, walking3Rect }, false);
    AnimationState* walkingLeft = AnimationBuilder::AssembleAnimationState(WALKING_LEFT, Duration::FromSeconds(1.5), spriteSheet, std::vector<sf::IntRect> { walking1Rect, walking2Rect, walking3Rect }, true);

    this->animator = new Animator(std::vector<AnimationState*> { idle, walkingRight, walkingLeft });
    this->animator->switchState(IDLE);

    this->updateCharacterDimensions();

    sf::Sprite* sprite = new sf::Sprite(*this->animator->getActiveTexture());
    this->sprite = sprite;
}

void Mario::updateCharacterDimensions() {
    sf::Vector2u size = this->animator->getActiveTexture()->getSize();

    this->width = Distance::FromMeters(size.x);
    this->height = Distance::FromMeters(size.y);
}

Collider Mario::getCollider() {
    return { this->position, this->width, this->height };
}

void Mario::onCollision(ICollidable *collider, Collision collision) {
    if(collider->getTag() == LEVEL_END) {
        this->hasReachedSceneEnding = true;
    }

    if(collider->getTag() == PLANT) {
        this->isDead = true;
    }

    if(collider->getTag() == COIN) {
        this->scoreBoard->increaseCoins();
    }

    if(collider->getTag() == MUSHROOM || collider->getTag() == FLOWER) {
        this->speedIncreases++;
    }

    if(collider->getTag() == BLOCK || collider->getTag() == QUESTION) {
        Collider colliderData = collider->getCollider();

        switch(collision.side) {
            case TOP: {
                this->position = Point::FromCartesian(this->position.getX(), colliderData.location.getY() + colliderData.height.toMeters());
                this->verticalVelocity = Point::FromCartesian(0, 0);
                break;
            }
            case BOTTOM: {
                this->position = Point::FromCartesian(this->position.getX(), colliderData.location.getY() - this->height.toMeters());
                this->isJumping = false;
                break;
            }
            case RIGHT: {
                this->position = Point::FromCartesian(colliderData.location.getX() - this->width.toMeters() + 1, this->position.getY());
                break;
            }
            case LEFT: {
                this->position = Point::FromCartesian(colliderData.location.getX() + colliderData.width.toMeters() - 1, this->position.getY());
                break;
            }
            case NONE: {
                break;
            }
        }
    }
}

std::string Mario::getTag() {
    return PLAYER;
}

void Mario::update(Duration deltaTime, Input input) {
    Point movement = Point::FromCartesian(0, FALL_SPEED * deltaTime.toMilliseconds());

    if(this->isJumping) {
        movement = movement - this->verticalVelocity;

        this->verticalVelocity = this->verticalVelocity - Point::FromCartesian(0, DECELERATION * deltaTime.toMilliseconds());
        if(this->verticalVelocity.getY() < 0) {
            this->verticalVelocity = Point::FromCartesian(0, 0);
        }
    }

    bool jump = input.getInputState(sf::Keyboard::Space);
    bool left = input.getInputState(sf::Keyboard::A);
    bool right = input.getInputState(sf::Keyboard::D);

    double speed = this->speedIncreases * SPEED_INCREASE;

    if(!this->isJumping && jump) {
        this->verticalVelocity = Point::FromCartesian(0, JUMP_SPEED * deltaTime.toMilliseconds());
        this->isJumping = true;
        movement = Point::FromCartesian(0, -1);
    }

    if(left) {
        this->animator->switchState(WALKING_LEFT);

        movement = movement - Point::FromCartesian(speed * MOVE_SPEED * deltaTime.toMilliseconds(), 0);
    }

    if(right) {
        this->animator->switchState(WALKING_RIGHT);

        movement = movement + Point::FromCartesian(speed * MOVE_SPEED * deltaTime.toMilliseconds(), 0);
    }

    if(!this->isJumping && !jump && !left && !right) {
        this->animator->switchState(IDLE);
    }

    this->position = this->position + movement;

    this->limitPositionToLowerBound();
    this->updateCharacterDimensions();

    this->animator->update(deltaTime);
}

void Mario::reset() {
    this->lowestPosition = Point::FromCartesian(0, 0);
    this->position = Point::FromCartesian(0, 0);
    this->verticalVelocity = Point::FromCartesian(0, 0);
    this->isDead = false;
    this->isJumping = false;
    this->hasReachedSceneEnding = false;
    this->speedIncreases = 1;
    this->animator->switchState(IDLE);
}

void Mario::limitPositionToLowerBound() {
    if(this->position.getX() < this->lowestPosition.getX()) {
        this->position = Point::FromCartesian(this->lowestPosition.getX(), this->position.getY());
    }

    if(this->position.horizontalDistanceTo(this->lowestPosition) > DISTANCE_TO_LOWERBOUND) {
        this->lowestPosition = Point::FromCartesian(this->position.getX() - DISTANCE_TO_LOWERBOUND.toMeters(), 0);
    }
}

void Mario::render(sf::RenderWindow &window) {
    this->sprite->setPosition(this->position.getX(), this->position.getY());
    this->sprite->setTexture(*this->animator->getActiveTexture());

    window.draw(*this->sprite);
}

Point Mario::getLowerBoundPosition() {
    return this->lowestPosition;
}

bool Mario::isAlive() {
    return !this->isDead;
}

bool Mario::hasCompletedLevel() {
    return this->hasReachedSceneEnding;
}