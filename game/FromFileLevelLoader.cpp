#include "FromFileLevelLoader.h"
#include "LevelAssembler.h"
#include <fstream>

Level FromFileLevelLoader::loadLevel(std::string name) {

    std::ifstream sMapFile(name);
    if(sMapFile.is_open()) {
        std::vector<std::vector<Blocks>> blocks;

        std::string name;
        sMapFile >> name;

        int width, height, blockSize;
        sMapFile >> width >> height >> blockSize;

        int block;
        for(int x = 0; x < width; x++) {
            std::vector<Blocks> column;
            for(int y = 0; y < height; y++) {
                sMapFile >> block;

                column.push_back((Blocks) block);
            }

            blocks.push_back(column);
        }

        Level level = LevelAssembler::AssembleFromBlocks(name, blocks, Distance::FromMeters(blockSize));

        return level;
    }
}
