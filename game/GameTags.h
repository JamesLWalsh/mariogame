#pragma once
#include <string>

const std::string COIN = "COIN";
const std::string BLOCK = "BLOCK";
const std::string PLAYER = "PLAYER";
const std::string MUSHROOM = "MUSHROOM";
const std::string QUESTION = "QUESTION";
const std::string FLOWER = "FLOWER";
const std::string PLANT = "PLANT";
const std::string LEVEL_END = "LEVEL_END";