#include "CoinBlock.h"
#include "GameTags.h"
#include "Textures.h"
#include "Coin.h"

Collider CoinBlock::getCollider() {
    return { this->position, this->size, this->size };
}

void CoinBlock::onCollision(ICollidable *collider, Collision collision) {
    if(collision.side == TOP && collider->getTag() == PLAYER) {
        this->hasBeenTriggered = true;
    }
}

std::string CoinBlock::getTag() {
    return BLOCK;
}

int CoinBlock::getTexture() {
    return T_COIN;
}

CoinBlock::CoinBlock(Point position, Distance size) : position(position), size(size) {}

void CoinBlock::update(EntityManager *entityManager, CollisionHandler* collisionHandler) {
    if(this->hasBeenTriggered && !this->hasSpawnedCoin) {
        Coin* coin = new Coin(this->position - Point::FromCartesian(0, 32));
        entityManager->addVisibleEntity(coin);
        collisionHandler->addDynamicCollider(coin);

        this->hasSpawnedCoin = true;
    }
}
