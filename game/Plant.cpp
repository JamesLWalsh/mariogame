#include "Plant.h"
#include "GameTags.h"
#include "Textures.h"
#include <time.h>

Plant::Plant(Point position, Distance size) : position(position), size(size) {}

Collider Plant::getCollider() {
    return { this->position, this->size, this->size };
}

void Plant::onCollision(ICollidable *collider, Collision collision) {}

std::string Plant::getTag() {
    return PLANT;
}

int Plant::getTexture() {
    Duration currentTime = Duration::FromSeconds(clock() / CLOCKS_PER_SEC);

    if(currentTime - this->lastSwitchTime > this->textureDuration) {
        this->isMouthOpened = !this->isMouthOpened;
        this->lastSwitchTime = currentTime;
    }

    return this->isMouthOpened ? T_PLANT_OPENED : T_PLANT_CLOSED;
}
