#pragma once

enum Textures {
    T_AIR = 32,
    T_BRICK = 2,
    T_QUESTION_1 = 7,
    T_QUESTION_2 = 8,
    T_QUESTION_3 = 9,
    T_QUESTION_TRIGGERED = 2,
    T_COIN = 3,
    T_PLANT_OPENED = 12,
    T_PLANT_CLOSED = 13,
    T_CASTLE = 14,
    T_SCENEJUMP = 15
};