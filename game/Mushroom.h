#pragma once

#include "VisibleEntity.h"

class Mushroom : public VisibleEntity {
public:
    Mushroom(Point spawnPoint);
    ~Mushroom();
    void update(Duration deltaTime);
    void render(sf::RenderWindow &window);
    void onCollision(ICollidable *collider, Collision collision);
    bool isTerminated();
    std::string getTag();
    Collider getCollider();

private:
    const double MOVE_SPEED = 0.02;
    const double FALL_SPEED = 0.05;
    const double RAISE_SPEED = 0.02;

    bool hasBeenTaken = false;
    bool hasGoneUp = false;
    Point verticalMovement = Point::FromCartesian(0, 0);

    sf::Sprite* sprite;
    Point direction = Point::FromCartesian(0, 0);
    Distance size = Distance::FromMeters(0);
};