#pragma once

#include "Level.h"
#include "Blocks.h"

class LevelAssembler {
public:
    static Level AssembleFromBlocks(std::string name, std::vector<std::vector<Blocks>> blocks, Distance blockSize);
};