#pragma once

#include <SFML/Graphics.hpp>

class TextureUtils {
public:
    static sf::Texture* LoadFromImage(sf::Image image, sf::IntRect cropRect, bool flipped);
};