#pragma once

#include <SFML/Graphics/Texture.hpp>
#include "AnimationState.h"

class Animator {
public:
    Animator(std::vector<AnimationState*> animations);
    void update(Duration deltaTime);
    void switchState(int stateCode);
    sf::Texture* getActiveTexture();
private:
    std::vector<AnimationState*> animationStates;
    AnimationState* currentState;
    Duration timeSinceSwitch = Duration::FromSeconds(0);
};


