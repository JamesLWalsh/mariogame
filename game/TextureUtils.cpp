#include "TextureUtils.h"

sf::Texture* TextureUtils::LoadFromImage(sf::Image image, sf::IntRect cropRect, bool flipped) {
    sf::IntRect newCropRect = cropRect;

    if(flipped) {
        image.flipHorizontally();
        sf::Vector2u imageSize = image.getSize();
        newCropRect = sf::IntRect(imageSize.x - cropRect.left - cropRect.width, cropRect.top, cropRect.width, cropRect.height);
    }

    sf::Texture* texture = new sf::Texture;
    texture->loadFromImage(image, newCropRect);

    return texture;
}