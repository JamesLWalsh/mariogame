#pragma once

#include "Animation.h"
#include "../engine/units/Duration.h"

typedef struct AnimationState {
    Animation* currentAnimation;
    Duration animationDuration = Duration::FromSeconds(0);
    int stateName;
} AnimationState;