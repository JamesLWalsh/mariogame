#include <iostream>
#include "Game.h"

#define MAP_WIDTH 32
#define MAP_HEIGHT 16

Game::Game(ITileRenderer* tileRenderer, Input* input, ILevelLoader* levelLoader, sf::RenderWindow &window) {
    CollisionHandler* collisionHandler = new CollisionHandler(Distance::FromMeters(100), Distance::FromMeters(16), true);
    this->collisionHandler = collisionHandler;

    this->tileRenderer = tileRenderer;
    this->levelLoader = levelLoader;
    this->input = input;
    this->scoreBoard = new ScoreBoard;

    this->mario = new Mario(this->scoreBoard);

    this->view = sf::FloatRect(0, 0, window.getSize().x, window.getSize().y);
    this->entityManager = new EntityManager(collisionHandler);
}

void Game::start(LevelList *levels) {
    this->level = levels;
    this->playLevel(level->name);
}

void Game::playLevel(std::string levelName) {
    Level level = this->levelLoader->loadLevel(levelName);

    this->clearLevel();
    this->map = level.map;

    for(auto &collider : level.staticColliders) {
        this->collisionHandler->addStaticCollider(collider);
    }

    for(auto &collider : level.dynamicColliders) {
        this->collisionHandler->addDynamicCollider(collider);
    }
    collisionHandler->addDynamicCollider(this->mario);

    for(auto &entity : level.visibleEntities) {
        this->entityManager->addVisibleEntity(entity);
    }

    this->scoreBoard->switchWorld(level.name);
    this->currentLevel = level;
}

void Game::clearLevel() {
    this->collisionHandler->clearDynamicColliders();
    this->collisionHandler->clearStaticColliders();

    this->entityManager->clearEntities();
    this->mario->reset();
    this->scoreBoard->reset();
}

void Game::render(sf::RenderWindow &window) {
    Point marioPosition = this->mario->getLowerBoundPosition();

    this->view.left = marioPosition.getX();

    window.setView(sf::View(this->view));

    this->background.render(window, marioPosition, this->getViewport());
    this->scoreBoard->render(window, marioPosition);

    Viewport view = { Point::FromCartesian(marioPosition.getX() / 32, 0), Distance::FromMeters(MAP_WIDTH + 1), Distance::FromMeters(MAP_HEIGHT) };

    this->entityManager->renderVisibleEntities(window);
    this->map->render(this->tileRenderer, Point::FromCartesian(0, 0), view);
    this->mario->render(window);
}

void Game::nextLevel() {
    if(this->level->next == nullptr) {
        this->scoreBoard->onGameComplete();
    } else {
        this->level = this->level->next;
        this->playLevel(this->level->name);
    }
}

void Game::update(Duration deltaTime) {
    if(!this->mario->isAlive()) {
        this->playLevel(this->level->name);
    }

    if(this->mario->hasCompletedLevel()) {
        this->nextLevel();
    }

    this->mario->update(deltaTime, *this->input);
    this->collisionHandler->performCollisions();
    this->entityManager->update(deltaTime, this->getViewport());

    for(auto &entitySpawner : this->currentLevel.entitySpawners) {
        entitySpawner->update(this->entityManager, this->collisionHandler);
    }
}

Viewport Game::getViewport() {
    return { Point::FromCartesian(this->view.left, this->view.top), Distance::FromMeters(this->view.width), Distance::FromMeters(this->view.height) };
}