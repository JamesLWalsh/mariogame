#pragma once

#include <SFML/Graphics.hpp>
#include "../engine/ICollidable.h"
#include "../engine/units/Duration.h"
#include "../engine/Input.h"
#include "Animator.h"
#include "EntityManager.h"
#include "ScoreBoard.h"

class Mario: public ICollidable {
public:
    Mario(ScoreBoard* scoreBoard);
    Collider getCollider() override;
    void onCollision(ICollidable *collider, Collision collision) override;
    std::string getTag() override;
    void update(Duration deltaTime, Input input);
    void render(sf::RenderWindow &window);
    void reset();
    bool isAlive();
    bool hasCompletedLevel();
    Point getLowerBoundPosition();
private:
    void updateCharacterDimensions();
    void limitPositionToLowerBound();

    const double MOVE_SPEED = 0.04;
    const double FALL_SPEED = 0.098;
    const double DECELERATION = 0.0004;
    const double JUMP_SPEED = 0.2;
    const double SPEED_INCREASE = 1.13;

    bool hasReachedSceneEnding = false;
    bool isDead = false;
    int speedIncreases = 1;
    Animator* animator;
    sf::Sprite* sprite;
    Point position;
    Point lowestPosition;
    Distance width;
    Distance height;

    ScoreBoard* scoreBoard;

    Distance DISTANCE_TO_LOWERBOUND = Distance::FromMeters(800);
    bool isJumping = false;
    Point verticalVelocity = Point::FromCartesian(0, 0);
};