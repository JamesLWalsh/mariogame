#pragma once

#include "../engine/units/Duration.h"
#include "EntityManager.h"

class Spawner {
public:
    virtual void update(EntityManager* entityManager, CollisionHandler* collisionHandler) = 0;
};