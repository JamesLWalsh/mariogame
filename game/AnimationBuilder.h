#pragma once

#include <SFML/Graphics.hpp>
#include "Animation.h"
#include "AnimationState.h"
#include "../engine/units/Duration.h"

class AnimationBuilder {
public:
    static AnimationState* AssembleAnimationState(int stateCode, Duration animationDuration, sf::Image spriteSheet, std::vector<sf::IntRect> cropRects, bool flipped);
    static Animation* AssembleAnimation(sf::Image spriteSheet, sf::IntRect rect, bool flipped);
};
