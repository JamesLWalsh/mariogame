#include <SFML/Graphics/Texture.hpp>
#include <math.h>
#include "Background.h"

Background::Background() {
    sf::Texture* backgroundTexture = new sf::Texture;
    backgroundTexture->loadFromFile("resources/maps/background.png");

    sf::Sprite backgroundSprite(*backgroundTexture);
    this->background = backgroundSprite;
    this->backgroundWidth = Distance::FromMeters(backgroundTexture->getSize().x);
}

void Background::render(sf::RenderWindow &window, Point position, Viewport viewport) {
    double startX = floor(viewport.start.getX() / this->backgroundWidth.toMeters());
    double endX = floor((viewport.start.getX() + viewport.width.toMeters()) / this->backgroundWidth.toMeters());

    this->background.setPosition(startX * this->backgroundWidth.toMeters(), 0);
    window.draw(this->background);

    for(int i = startX + 1; i <= endX; i++) {
        this->background.setPosition(i * this->backgroundWidth.toMeters(), 0);
        window.draw(this->background);
    }
}