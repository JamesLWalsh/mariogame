#include "LevelAssembler.h"
#include "../engine/tiles/SimpleTile.h"
#include "BlockCollider.h"
#include "CoinBlock.h"
#include "Flower.h"
#include "NextLevelBlock.h"

Level LevelAssembler::AssembleFromBlocks(std::string name, std::vector<std::vector<Blocks>> blocks, Distance blockSize) {
    std::vector<std::vector<ITile*>> map;
    std::vector<ICollidable*> staticColliders;
    std::vector<ICollidable*> dynamicColliders;
    std::vector<VisibleEntity*> visibleEntities;
    std::vector<Spawner*> entitySpawners;

    for(int x = 0; x < blocks.size(); x++) {
        std::vector<ITile*> column;

        for(int y = 0; y < blocks[x].size(); y++) {
            int block = blocks[x][y];
            Point colliderPosition = Point::FromCartesian(x * blockSize.toMeters(), y * blockSize.toMeters());

            switch(block) {
                case AIR: {
                    ITile *airTile = new SimpleTile(T_AIR);
                    column.push_back(airTile);

                    break;
                }
                case BRICK: {
                    ITile *brickTile = new SimpleTile(T_BRICK);
                    column.push_back(brickTile);

                    BlockCollider *brickCollider = new BlockCollider(colliderPosition, blockSize);
                    staticColliders.push_back(brickCollider);

                    break;
                }
                case QUESTION: {
                    QuestionBlock* questionBlock = new QuestionBlock(colliderPosition, blockSize);
                    column.push_back(questionBlock);
                    staticColliders.push_back(questionBlock);
                    entitySpawners.push_back(questionBlock);

                    break;
                }
                case COIN: {
                    CoinBlock* coinBlock = new CoinBlock(colliderPosition, blockSize);
                    column.push_back(coinBlock);
                    staticColliders.push_back(coinBlock);
                    entitySpawners.push_back(coinBlock);

                    break;
                }
                case FLOWER: {
                    Flower* flower = new Flower(colliderPosition);
                    dynamicColliders.push_back(flower);
                    visibleEntities.push_back(flower);

                    ITile* airTile = new SimpleTile(T_AIR);
                    column.push_back(airTile);

                    break;
                }
                case PLANT: {
                    Plant* plant = new Plant(colliderPosition, blockSize);
                    staticColliders.push_back(plant);
                    column.push_back(plant);

                    break;
                }
                case CASTLE: {
                    NextLevelBlock* nextLevelBlock = new NextLevelBlock(T_CASTLE, colliderPosition, blockSize);
                    column.push_back(nextLevelBlock);
                    staticColliders.push_back(nextLevelBlock);

                    break;
                }
                case SCENEJUMP: {
                    NextLevelBlock* nextLevelBlock = new NextLevelBlock(T_SCENEJUMP, colliderPosition, blockSize);
                    column.push_back(nextLevelBlock);
                    staticColliders.push_back(nextLevelBlock);

                    break;
                }
                default: {
                    break;
                }
            }
        }

        map.push_back(column);
    }

    TileMap* tileMap = new TileMap(map);

    return { staticColliders, dynamicColliders, visibleEntities, entitySpawners, tileMap, name };
}