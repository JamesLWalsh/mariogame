#pragma once

#include <SFML/Graphics.hpp>
#include "../engine/units/Duration.h"
#include "../engine/units/Distance.h"
#include "../engine/units/Point.h"
#include "../engine/Viewport.h"
#include "../engine/ICollidable.h"

class Entity {
public:
    ~Entity() {};
    virtual void update(Duration deltaTime) = 0;
    virtual void render(sf::RenderWindow &window) = 0;
};