#pragma once

#include "Level.h"

class ILevelLoader {
public:
    virtual ~ILevelLoader() {};
    virtual Level loadLevel(std::string name) = 0;
};