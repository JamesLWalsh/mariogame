#pragma once
#include <SFML/Graphics.hpp>

typedef struct Animation {
    sf::Texture* texture;
    struct Animation* next;
} Animation;