#include "Flower.h"
#include "GameTags.h"

Flower::Flower(Point position) : VisibleEntity(position, Distance::FromMeters(0), Distance::FromMeters(0)){
    sf::Texture* texture = new sf::Texture;
    texture->loadFromFile("resources/entities/flower.png");
    this->width = Distance::FromMeters(texture->getSize().x);
    this->height = Distance::FromMeters(texture->getSize().y);

    sf::Sprite flowerSprite(*texture);
    flowerSprite.setPosition(position.getX(), position.getY());
    this->flowerSprite = flowerSprite;
}

void Flower::update(Duration deltaTime) {}

void Flower::render(sf::RenderWindow &window) {
    window.draw(this->flowerSprite);
}

bool Flower::isTerminated() {
    return this->hasBeenTaken;
}

void Flower::onCollision(ICollidable *collider, Collision collision) {
    if(collider->getTag() == PLAYER) {
        this->hasBeenTaken = true;
    }
}

std::string Flower::getTag() {
    return FLOWER;
}
