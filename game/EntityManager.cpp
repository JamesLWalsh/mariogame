#include <iostream>
#include "EntityManager.h"

EntityManager::EntityManager(CollisionHandler *collisionHandler) {
    this->collisionHandler = collisionHandler;
}

void EntityManager::addEntity(Entity* entity) {
    this->entities.push_back(entity);
}

void EntityManager::addVisibleEntity(VisibleEntity *entity) {
    this->visibleEntities.push_back(entity);
}

void EntityManager::renderVisibleEntities(sf::RenderWindow &window) {
    for(auto &entity : this->visibleEntities) {
        entity->render(window);
    }
}

void EntityManager::update(Duration deltaTime, Viewport viewport) {
    this->removeInvisibleEntities(viewport);
    for(auto &visibleEntity : this->visibleEntities) {
        visibleEntity->update(deltaTime);
    }

    for(auto &entity : this->entities) {
        entity->update(deltaTime);
    }
}

void EntityManager::removeInvisibleEntities(Viewport viewport) {
    for(int i = this->visibleEntities.size() - 1; i >= 0; i--) {
        VisibleEntity* entity = this->visibleEntities[i];

        if(entity->isTerminated() || !entity->isVisible(viewport)) {
            this->collisionHandler->removeDynamicCollider(entity);
            this->visibleEntities.erase(this->visibleEntities.begin() + i);

            delete entity;
        }
    }
}

void EntityManager::clearEntities() {
    for(int i = this->visibleEntities.size() - 1; i >= 0; i--) {
        VisibleEntity* entity = this->visibleEntities[i];
        this->collisionHandler->removeDynamicCollider(entity);
        this->visibleEntities.erase(this->visibleEntities.begin() + i);

        delete entity;
    }

    for(int i = this->entities.size() - 1; i >= 0; i--) {
        Entity* entity = this->entities[i];
        this->entities.erase(this->entities.begin() + i);

        delete entity;
    }
}
