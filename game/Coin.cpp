#include "Coin.h"
#include "GameTags.h"

Coin::Coin(Point spawnPosition) : VisibleEntity(spawnPosition, Distance::FromMeters(0), Distance::FromMeters(0)) {
    sf::Texture* coinTexture = new sf::Texture;
    coinTexture->loadFromFile("resources/entities/coin.png");

    sf::Sprite coinSprite(*coinTexture);
    coinSprite.setPosition(spawnPosition.getX(), spawnPosition.getY());
    this->coinSprite = coinSprite;


    this->width = Distance::FromMeters(coinTexture->getSize().x);
    this->height = Distance::FromMeters(coinTexture->getSize().y);
}

void Coin::update(Duration deltaTime) {}

void Coin::render(sf::RenderWindow &window) {
    window.draw(this->coinSprite);
}

bool Coin::isTerminated() {
    return this->collected;
}

void Coin::onCollision(ICollidable *collider, Collision collision) {
    if(collider->getTag() == PLAYER) {
        this->collected = true;
    }
}

std::string Coin::getTag() {
    return COIN;
}
