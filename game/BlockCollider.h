#pragma once

#include "../engine/ICollidable.h"
#include "EntityManager.h"

class BlockCollider : public ICollidable {
public:
    BlockCollider(Point position, Distance size);
    Collider getCollider();
    void onCollision(ICollidable *collider, Collision collision);
    std::string getTag();
private:
    Point position;
    Distance size;
};