#pragma once

#include "../engine/CollisionHandler.h"
#include "VisibleEntity.h"

class EntityManager {
public:
    EntityManager(CollisionHandler* collisionHandler);
    void addEntity(Entity* entity);
    void addVisibleEntity(VisibleEntity* entity);
    void renderVisibleEntities(sf::RenderWindow &window);
    void update(Duration deltaTime, Viewport viewport);
    void clearEntities();
private:
    CollisionHandler* collisionHandler;
    void removeInvisibleEntities(Viewport viewport);
    std::vector<Entity*> entities;
    std::vector<VisibleEntity*> visibleEntities;
};