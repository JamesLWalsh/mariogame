#pragma once

#include "../engine/units/Point.h"
#include "../engine/units/Duration.h"
#include "../engine/Viewport.h"
#include "Entity.h"

class VisibleEntity : public Entity, public ICollidable {
public:
    VisibleEntity(Point spawnPoint, Distance width, Distance height);
    virtual void update(Duration deltaTime) = 0;
    virtual void render(sf::RenderWindow &window) = 0;
    virtual bool isTerminated() = 0;
    bool isVisible(Viewport viewport);
    Collider getCollider();
    virtual void onCollision(ICollidable *collider, Collision collision) = 0;
    virtual std::string getTag() = 0;

protected:
    Point position;
    Distance width;
    Distance height;
};