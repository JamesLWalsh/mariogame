#include "AnimationBuilder.h"
#include "TextureUtils.h"

AnimationState* AnimationBuilder::AssembleAnimationState(int stateCode, Duration animationDuration, sf::Image spriteSheet,
                                                         std::vector<sf::IntRect> cropRects, bool flipped) {
    Animation* start = nullptr;
    Animation* previous = nullptr;

    for(auto &cropRect : cropRects) {
        Animation* animation = AnimationBuilder::AssembleAnimation(spriteSheet, cropRect, flipped);

        if(start == nullptr) {
            start = animation;
        }

        if(previous != nullptr) {
            previous->next = animation;
        }

        previous = animation;
    }

    if(previous != nullptr) {
        previous->next = start;
    }

    AnimationState* state = new AnimationState;
    state->stateName = stateCode;
    state->currentAnimation = start;
    state->animationDuration = animationDuration;

    return state;
}

Animation* AnimationBuilder::AssembleAnimation(sf::Image spriteSheet, sf::IntRect rect, bool flipped) {
    sf::Texture* texture = TextureUtils::LoadFromImage(spriteSheet, rect, flipped);

    Animation* animation = new Animation;
    animation->texture = texture;
    animation->next = nullptr;

    return animation;
}