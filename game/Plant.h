#pragma once

#include "../engine/ITile.h"
#include "../engine/ICollidable.h"
#include "../engine/units/Duration.h"

class Plant : public ITile, public ICollidable {
public:
    Plant(Point position, Distance size);
    Collider getCollider();
    void onCollision(ICollidable *collider, Collision collision);
    std::string getTag();
    int getTexture();
private:
    Point position;
    Distance size;

    bool isMouthOpened = false;
    Duration lastSwitchTime = Duration::FromSeconds(0);
    Duration textureDuration = Duration::FromSeconds(1);
};