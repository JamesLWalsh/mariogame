#include "ScoreBoard.h"
#include <string>
#include <sstream>
#include <iostream>

ScoreBoard::ScoreBoard() {
    sf::Font font;
    font.loadFromFile("resources/fonts/SuperMario256.ttf");

    this->renderFont = font;

    sf::Text worldText("", this->renderFont, 20);
    worldText.setColor(sf::Color::White);
    worldText.setPosition(200, 8);
    this->worldText = worldText;

    sf::Text coinsText("", this->renderFont, 20);
    coinsText.setColor(sf::Color::White);
    coinsText.setPosition(425, 8);
    coinsText.setString("x0");
    this->coinsText = coinsText;

    sf::Texture* coinTexture = new sf::Texture;
    coinTexture->loadFromFile("resources/coin.png");

    sf::Sprite coin(*coinTexture);
    coin.setPosition(400, 10);
    coin.setScale(0.7, 0.7);

    sf::Text gameCompletedText("Bravo! Vous avez fini Mario Bros knockoff!", this->renderFont, 27);
    gameCompletedText.setPosition(500, 200);
    this->gameCompletedText = gameCompletedText;

    this->coin = coin;
}

void ScoreBoard::onGameComplete() {
    this->hasCompletedGame = true;
}

void ScoreBoard::increaseCoins() {
    this->coins++;
    std::ostringstream ss;
    ss << this->coins;
    this->coinsText.setString("x" + ss.str());
}

void ScoreBoard::switchWorld(std::string newWorld) {
    this->worldText.setString("World " + newWorld);
    this->reset();
}

void ScoreBoard::render(sf::RenderWindow &window, Point lowerBound) {
    if(!this->hasCompletedGame) {
        this->worldText.setPosition(lowerBound.getX() + WORLD_TEXT_POSITION.getX(), WORLD_TEXT_POSITION.getY());
        this->coinsText.setPosition(lowerBound.getX() + COINS_TEXT_POSITION.getX(), COINS_TEXT_POSITION.getY());
        this->coin.setPosition(lowerBound.getX() + COINS_ICON_POSITION.getX(), COINS_ICON_POSITION.getY());

        window.draw(this->worldText);
        window.draw(this->coinsText);
        window.draw(this->coin);
    } else {
        this->gameCompletedText.setPosition(lowerBound.getX() + GAME_COMPLETED_TEXT_POSITION.getX(), GAME_COMPLETED_TEXT_POSITION.getY());
        window.draw(this->gameCompletedText);
    }
}

void ScoreBoard::reset() {
    this->coins = 0;
    this->coinsText.setString("x0");
}