#include "NextLevelBlock.h"
#include "GameTags.h"

NextLevelBlock::NextLevelBlock(Textures texture, Point position, Distance size) : position(position), size(size) {
    this->texture = texture;
}

Collider NextLevelBlock::getCollider() {
    return { this->position, this->size, this->size };
}

void NextLevelBlock::onCollision(ICollidable *collider, Collision collision) {}

std::string NextLevelBlock::getTag() {
    return LEVEL_END;
}

int NextLevelBlock::getTexture() {
    return this->texture;
}
