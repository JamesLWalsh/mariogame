#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include "../engine/units/Duration.h"
#include "Level.h"
#include "../engine/CollisionHandler.h"
#include "../engine/Input.h"
#include "Mario.h"
#include "ScoreBoard.h"
#include "EntityManager.h"
#include "Background.h"
#include "ILevelLoader.h"

class Game {
public:
    Game(ITileRenderer* tileRenderer, Input* input, ILevelLoader* levelLoader, sf::RenderWindow &window);
    void update(Duration deltaTime);
    void render(sf::RenderWindow &window);
    void start(LevelList* levels);
    void togglePause();
private:
    void nextLevel();
    void playLevel(std::string levelName);
    void clearLevel();
    Viewport getViewport();
    sf::FloatRect view;
    CollisionHandler* collisionHandler;
    ITileRenderer* tileRenderer;
    ILevelLoader* levelLoader;
    Input* input;

    LevelList* level;
    Background background;
    Level currentLevel;
    EntityManager* entityManager;
    TileMap* map;
    Mario* mario;
    ScoreBoard* scoreBoard;
};