#pragma once

#include "../engine/ITile.h"
#include "../engine/ICollidable.h"
#include "../engine/tiles/AnimatedTile.h"
#include "Textures.h"
#include "EntityManager.h"
#include "Spawner.h"

class QuestionBlock: public ICollidable, public ITile, public Spawner {
public:
    QuestionBlock(Point position, Distance size);
    Collider getCollider() override;
    void onCollision(ICollidable *collider, Collision collision) override;
    std::string getTag() override;
    int getTexture() override;
    void update(EntityManager* entityManager, CollisionHandler* collisionHandler);

private:
    bool triggered = false;
    bool hasSpawnedMushroom = false;

    Point position;
    Distance size;
    std::vector<int> textures = { T_QUESTION_1, T_QUESTION_2, T_QUESTION_3 };
    int currentTextureIndex = 0;
    Duration lastSwitchTime = Duration::FromSeconds(0);
    Duration textureDuration = Duration::FromSeconds(0.3);
};