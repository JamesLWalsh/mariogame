#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "../engine/units/Point.h"
#include "../engine/Viewport.h"

class Background {
public:
    Background();
    void render(sf::RenderWindow &window, Point position, Viewport viewport);
private:
    Distance backgroundWidth = Distance::FromMeters(0);
    sf::Sprite background;
};