#include "../engine/ICollidable.h"
#include "../engine/ITile.h"
#include "EntityManager.h"
#include "Spawner.h"

class CoinBlock : public ICollidable, public ITile, public Spawner {
public:
    CoinBlock(Point position, Distance size);
    Collider getCollider();
    void onCollision(ICollidable *collider, Collision collision);
    void update(EntityManager* entityManager, CollisionHandler* collisionHandler);
    std::string getTag();
    int getTexture();
private:
    Point position;
    Distance size;
    bool hasSpawnedCoin = false;
    bool hasBeenTriggered = false;
};
