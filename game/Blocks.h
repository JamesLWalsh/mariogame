#pragma once

enum Blocks {
    AIR,
    BRICK,
    COIN,
    QUESTION,
    FLOWER,
    PLANT,
    CASTLE,
    SCENEJUMP
};