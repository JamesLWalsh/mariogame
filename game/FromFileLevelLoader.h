#pragma once

#include "ILevelLoader.h"

class FromFileLevelLoader : public ILevelLoader {
public:
    Level loadLevel(std::string name);
};