#pragma once

#include "../engine/ITile.h"
#include "../engine/ICollidable.h"
#include "Textures.h"

class NextLevelBlock : public ITile, public ICollidable {
public:
    NextLevelBlock(Textures texture, Point position, Distance size);
    Collider getCollider();
    void onCollision(ICollidable *collider, Collision collision);
    std::string getTag();
    int getTexture();
private:
    Textures texture;
    Point position;
    Distance size;
};