#pragma once

#include "VisibleEntity.h"

class Flower : public VisibleEntity {
public:
    Flower(Point position);
    virtual void update(Duration deltaTime);

    virtual void render(sf::RenderWindow &window);

    virtual bool isTerminated();

    virtual void onCollision(ICollidable *collider, Collision collision);

    virtual std::string getTag();
private:
    bool hasBeenTaken = false;
    sf::Sprite flowerSprite;
};