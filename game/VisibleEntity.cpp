#include "VisibleEntity.h"
#include "../engine/CollisionEvaluator.h"

VisibleEntity::VisibleEntity(Point spawnPoint, Distance width, Distance height) : position(spawnPoint), width(width), height(height) {}

bool VisibleEntity::isVisible(Viewport viewport) {
    Collider entityCollider = { this->position, this->width, this->height };
    Collider windowCollider = { viewport.start, viewport.width, viewport.height };

    Collision result = CollisionEvaluator::Evaluate(entityCollider, windowCollider);

    return result.side != NONE;
}

Collider VisibleEntity::getCollider() {
    return { this->position, this->width, this->height };
}