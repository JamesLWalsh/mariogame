#include <SFML/Graphics.hpp>
#include <iostream>
#include "engine/TileMap.h"
#include "sfml/SFMLTileSet.h"
#include "sfml/SFMLTileRenderer.h"
#include "engine/tiles/AnimatedTile.h"
#include "engine/Input.h"
#include "game/Game.h"
#include "game/FromFileLevelLoader.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1024, 512), "Mario Bros knockoff by James L. Walsh");

    sf::Image tileSetImage;
    if(!tileSetImage.loadFromFile("resources/tilesets/tileset.png")) {
        std::cout << "Could not load the tileset" << std::endl;

        return 0;
    };

    SFMLTileSet tileSet(tileSetImage, 32);
    ITileRenderer* tileRenderer = new SFMLTileRenderer(tileSet, &window);

    sf::Clock clock;
    Duration deltaTime = Duration::FromMilliseconds(16);
    Duration lastUpdate = Duration::FromSeconds(clock.getElapsedTime().asSeconds());
    Duration zero = Duration::FromSeconds(0);

    Input* input = new Input();
    ILevelLoader* levelLoader = new FromFileLevelLoader();

    LevelList* levelsStart = nullptr;
    LevelList* levelsEnd = nullptr;

    LevelList* level = new LevelList;
    level->name = "resources/maps/new_castle.tm";
    level->next = nullptr;
    levelsStart = levelsEnd = level;

    level = new LevelList;
    level->name = "resources/maps/beach_club.tm";
    level->next = nullptr;
    levelsStart->next = level;
    levelsEnd = level;

    Game game(tileRenderer, input, levelLoader, window);
    game.start(levelsStart);

    bool isPaused = false;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if(event.type == sf::Event::KeyPressed) {
                if(event.key.code == sf::Keyboard::P) {
                    isPaused = !isPaused;
                }

                input->setInputState(event.key.code, true);
            }

            if(event.type == sf::Event::KeyReleased) {
                input->setInputState(event.key.code, false);
            }
        }

        Duration currentTime = Duration::FromSeconds(clock.getElapsedTime().asSeconds());
        Duration timeSinceFrame = currentTime - lastUpdate;
        lastUpdate = currentTime;

        if(isPaused)
            continue;

        while(timeSinceFrame > zero) {
            game.update(deltaTime);

            timeSinceFrame = timeSinceFrame - deltaTime;
        }

        window.clear(sf::Color(94, 145, 254));
        game.render(window);
        window.display();

        clock.restart();
    }

    return 0;
}