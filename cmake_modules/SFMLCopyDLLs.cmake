if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    add_custom_command(TARGET MarioGame POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${SFML_INCLUDE_DIR}/../bin/ ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
endif()

